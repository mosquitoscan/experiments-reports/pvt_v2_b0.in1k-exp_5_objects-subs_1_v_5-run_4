
Model info for pvt_v2_b0.in1k
=============================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 32 x 56 x 56   
Conv2d                                    4736       True      
LayerNorm                                 64         True      
LayerNorm                                 64         True      
Linear                                    1056       True      
____________________________________________________________________________
                     64 x 49 x 64        
Linear                                    2112       True      
Linear                                    1056       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 32 x 7 x 7     
Conv2d                                    65568      True      
LayerNorm                                 64         True      
Identity                                                       
LayerNorm                                 64         True      
____________________________________________________________________________
                     64 x 3136 x 256     
Linear                                    8448       True      
Identity                                                       
Conv2d                                    2560       True      
GELU                                                           
____________________________________________________________________________
                     64 x 3136 x 32      
Linear                                    8224       True      
Dropout                                                        
Identity                                                       
LayerNorm                                 64         True      
Linear                                    1056       True      
____________________________________________________________________________
                     64 x 49 x 64        
Linear                                    2112       True      
Linear                                    1056       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 32 x 7 x 7     
Conv2d                                    65568      True      
LayerNorm                                 64         True      
Identity                                                       
LayerNorm                                 64         True      
____________________________________________________________________________
                     64 x 3136 x 256     
Linear                                    8448       True      
Identity                                                       
Conv2d                                    2560       True      
GELU                                                           
____________________________________________________________________________
                     64 x 3136 x 32      
Linear                                    8224       True      
Dropout                                                        
Identity                                                       
LayerNorm                                 64         True      
____________________________________________________________________________
                     64 x 64 x 28 x 28   
Conv2d                                    18496      True      
LayerNorm                                 128        True      
LayerNorm                                 128        True      
Linear                                    4160       True      
____________________________________________________________________________
                     64 x 49 x 128       
Linear                                    8320       True      
Linear                                    4160       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 64 x 7 x 7     
Conv2d                                    65600      True      
LayerNorm                                 128        True      
Identity                                                       
LayerNorm                                 128        True      
____________________________________________________________________________
                     64 x 784 x 512      
Linear                                    33280      True      
Identity                                                       
Conv2d                                    5120       True      
GELU                                                           
____________________________________________________________________________
                     64 x 784 x 64       
Linear                                    32832      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 128        True      
Linear                                    4160       True      
____________________________________________________________________________
                     64 x 49 x 128       
Linear                                    8320       True      
Linear                                    4160       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 64 x 7 x 7     
Conv2d                                    65600      True      
LayerNorm                                 128        True      
Identity                                                       
LayerNorm                                 128        True      
____________________________________________________________________________
                     64 x 784 x 512      
Linear                                    33280      True      
Identity                                                       
Conv2d                                    5120       True      
GELU                                                           
____________________________________________________________________________
                     64 x 784 x 64       
Linear                                    32832      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 128        True      
____________________________________________________________________________
                     64 x 160 x 14 x 14  
Conv2d                                    92320      True      
LayerNorm                                 320        True      
LayerNorm                                 320        True      
Linear                                    25760      True      
____________________________________________________________________________
                     64 x 49 x 320       
Linear                                    51520      True      
Linear                                    25760      True      
Dropout                                                        
____________________________________________________________________________
                     64 x 160 x 7 x 7    
Conv2d                                    102560     True      
LayerNorm                                 320        True      
Identity                                                       
LayerNorm                                 320        True      
____________________________________________________________________________
                     64 x 196 x 640      
Linear                                    103040     True      
Identity                                                       
Conv2d                                    6400       True      
GELU                                                           
____________________________________________________________________________
                     64 x 196 x 160      
Linear                                    102560     True      
Dropout                                                        
Identity                                                       
LayerNorm                                 320        True      
Linear                                    25760      True      
____________________________________________________________________________
                     64 x 49 x 320       
Linear                                    51520      True      
Linear                                    25760      True      
Dropout                                                        
____________________________________________________________________________
                     64 x 160 x 7 x 7    
Conv2d                                    102560     True      
LayerNorm                                 320        True      
Identity                                                       
LayerNorm                                 320        True      
____________________________________________________________________________
                     64 x 196 x 640      
Linear                                    103040     True      
Identity                                                       
Conv2d                                    6400       True      
GELU                                                           
____________________________________________________________________________
                     64 x 196 x 160      
Linear                                    102560     True      
Dropout                                                        
Identity                                                       
LayerNorm                                 320        True      
____________________________________________________________________________
                     64 x 256 x 7 x 7    
Conv2d                                    368896     True      
LayerNorm                                 512        True      
LayerNorm                                 512        True      
Linear                                    65792      True      
____________________________________________________________________________
                     64 x 49 x 512       
Linear                                    131584     True      
Linear                                    65792      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 512        True      
____________________________________________________________________________
                     64 x 49 x 1024      
Linear                                    263168     True      
Identity                                                       
Conv2d                                    10240      True      
GELU                                                           
____________________________________________________________________________
                     64 x 49 x 256       
Linear                                    262400     True      
Dropout                                                        
Identity                                                       
LayerNorm                                 512        True      
Linear                                    65792      True      
____________________________________________________________________________
                     64 x 49 x 512       
Linear                                    131584     True      
Linear                                    65792      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 512        True      
____________________________________________________________________________
                     64 x 49 x 1024      
Linear                                    263168     True      
Identity                                                       
Conv2d                                    10240      True      
GELU                                                           
____________________________________________________________________________
                     64 x 49 x 256       
Linear                                    262400     True      
Dropout                                                        
Identity                                                       
LayerNorm                                 512        True      
____________________________________________________________________________
                     64 x 256 x 1 x 1    
AdaptiveAvgPool2d                                              
AdaptiveMaxPool2d                                              
____________________________________________________________________________
                     64 x 512            
Flatten                                                        
BatchNorm1d                               1024       True      
Dropout                                                        
Linear                                    262144     True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 12             
Linear                                    6144       True      
____________________________________________________________________________

Total params: 3,680,096
Total trainable params: 3,680,096
Total non-trainable params: 0

Optimizer used: <function Adam at 0x79f839917e20>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback